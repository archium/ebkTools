// ebkTools/cli project ebkTools.go
package cli

import "os"

//type universal interface{}

func MapCLIArguments() *map[string]string {
	cliAguments := os.Args[1:] //without the program path itself

	//var cliAgumentsMapped map[string]universal = make(map[string]universal)
	var cliAgumentsMapped map[string]string = make(map[string]string)

	//Dismantle list of command line arguments
	for key, value := range cliAguments {
		if value[0:1] == "-" {
			if len(cliAguments) > key+1 && cliAguments[key+1][0:1] != "-" {
				cliAgumentsMapped[cliAguments[key]] = cliAguments[key+1] //; fmt.Printf("Argument mit Wert %v = %v\n", cliAguments[key], cliAguments[key+1])

			} else {
				//cliAgumentsMapped[cliAguments[key]] = true
				cliAgumentsMapped[cliAguments[key]] = "" //; fmt.Printf("Argument ohne Wert %v\n", cliAguments[key])
			}
		}

	}
	return &cliAgumentsMapped // test it with: fmt.Println((*cli.MapCLIArguments())["-a"])
}
