package cli

import (
	"reflect"
	"testing"
)

func TestMapCLIArguments(t *testing.T) {
	var ergebnisSoll1, ergebnisSoll2, ergebnisSoll3 map[string]string
	ergebnisSoll1 = make(map[string]string)
	ergebnisSoll2 = make(map[string]string)
	ergebnisSoll3 = map[string]string{
		"-test.cpuprofile=cpu.prof": "",
		"-test.memprofile=mem.prof": "",
		"-test.bench=.":             "",
		"-test.outputdir":           "./",
	}
	ergebnisIst := *MapCLIArguments()
	ergebnisSoll1["-test.v=true"] = ""
	ergebnisSoll2["-test.bench=.*"] = ""

	if !reflect.DeepEqual(ergebnisIst, ergebnisSoll1) && !reflect.DeepEqual(ergebnisIst, ergebnisSoll2) && !reflect.DeepEqual(ergebnisIst, ergebnisSoll3) {
		t.Error("Erwartung:", ergebnisSoll1, "Ergebnis:", ergebnisIst)
	}
}

//Namenskonvention: Benchmark…
func BenchmarkMapCLIArguments(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = *MapCLIArguments()
	}
}
