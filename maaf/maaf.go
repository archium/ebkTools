// maaf project maaf.go
package maaf

import "math"

func Round(number float64, digits int8) float64 {
	pow := math.Pow(10, float64(digits))
	return math.Floor(number*pow+.5) / pow
}
