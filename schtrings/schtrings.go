// schtrings project schtrings.go
package schtrings

import "bytes"

func Concatstring(args ...string) string {
	var buffer bytes.Buffer
	for _, v := range args {
		buffer.WriteString(v)
	}
	return buffer.String()
}

type Concatablestring string

func (f *Concatablestring) Join(args ...string) string {
	var buffer bytes.Buffer
	buffer.WriteString(string(*f))
	for _, v := range args {
		buffer.WriteString(v)
	}
	return buffer.String()
}

/*
package main

import (
	. "ebkTools/schtrings"
	"fmt"
)

func main() {
	fmt.Println(Concatstring("a", "b", "c"))

	teststring := (Concatablestring)("Anfangswert ")

	fmt.Println(Join("gefolgt ", "von ", "meinem ", "Text"))
}
*/
